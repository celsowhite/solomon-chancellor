$(document).ready(function(){

	/*=======================
	SUB NAV REVEAL
	=======================*/

	var showMenu = function() {
		$(this).find('.sub_nav').fadeIn(100);
	}

	var hideMenu = function() {
		$(this).find('.sub_nav').fadeOut(100);
	}

	$('.top_level_link').hover(showMenu, hideMenu);

	/*=======================
	PRODUCT FEATURES IMAGE TOGGLE
	=======================*/

	$('.feature_block').click(function(){

		var featureTitle = $(this).attr('data-title');
		var featureText = $('.product_features_text#' + featureTitle);

		// Only change the active states if the block that was clicked isn't currently active.

		if(!$(this).hasClass('active')) {

			// Remove the active state from the currently active image

			$('.feature_block.active').removeClass('active');

			// Add the active state to the clicked block

			$(this).addClass('active');

			// Change the active text

			$('.product_features_text.active').fadeOut(300, function(){
				featureText.fadeIn(300);
				$(this).removeClass('active');
				featureText.addClass('active');
			});

		}

		// Smooth scroll to the feature text on mobile.

		var productFeatures = $('.product_features_column');
		var productFeaturesOffset = productFeatures.offset().top - 50;
		var mq = window.matchMedia( "(max-width: 768px)" );
		if(mq.matches) {
			$('html, body').animate({scrollTop: productFeaturesOffset}, 1000);
		}

	});

	/*=======================
	SLIDER
	=======================*/

	$('.sc_slider').flexslider({
		animation: "fade",
		slideshow: true,
		slideshowSpeed: 5000,
		directionNav: false
	});

	/*================================= 
	FITVIDS
	=================================*/

	/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

	$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

	/*=== Target div for fitVids ===*/

	$(".video_embed").fitVids();

});